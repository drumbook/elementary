# Makefile to build the PDFs

FILENAME = drumbook
TEX = $(FILENAME).tex
PDF = $(FILENAME).pdf
FLAGS = -interaction=nonstopmode

.PHONY: $(PDF) all clean

# Run lilypond first
all: pdf inputs $(PDF)

pdf:
	for f in $$(ls *ly | cut -d \. -f 1); do make $$f.pdf; done

%.pdf: %.ly
	lilypond $<

# Add etude-*.pdf filenames to the inputs.tex file
inputs: $(wildcard /*.pdf)	
	ls ./etude-[0-9][0-9].pdf | awk '{printf "\\includepdf{%s}\n", $$1}' > inputs.tex

$(PDF): $(TEX)
	latexmk -pdf -pdflatex='pdflatex $(FLAGS)' -use-make $(TEX)

clean: latexmk -C
