% [[file:helper.org::*Header][Header:1]]
\include "definitions.ly"
\header {
  title = "6"
}
% Header:1 ends here

% [[file:helper.org::*Score][Score:1]]
%{
Initial input: \time 4/4 '(4 4 4 4 4 2 2 2 1 1), 16, 5
%}
\version "2.20.0"
\score {
  \drums {
    \time 4/4
    sn4 sn4 sn2
    sn4 r2.
    sn4 r4 sn2
    sn4 sn4 sn2
    \break
    sn4 sn4 sn4 sn4
    sn1
    sn4 sn4 sn4 sn4
    r4 sn4 r2
    \break
    sn4 sn4 sn2
    sn4 sn4 sn2
    r1
    sn4 sn4 r2
    \break
    sn4 sn4 sn2
    sn4 sn4 sn2
    sn4 sn4 sn2
    sn4 sn4 sn2
    \break
    sn4 sn4 sn2
    sn4 sn4 sn2
    sn4 sn4 r2
    sn4 sn4 sn4 sn4
    \break
    sn4 r4 sn2
    sn4 r4 sn2
    r2 sn2
    sn4 sn4 sn2
    \break
    sn4 sn4 sn2
    sn4 sn4 r2
    sn4 sn4 r2
    sn4 r4 sn2
    \break
    sn4 sn4 sn2
    sn4 r4 sn2
    r2 sn2
    sn4 r2.
    \break
    sn4 sn4 sn4 sn4
    sn2 r2
    r2 sn2
    sn4 sn4 sn2
    \break
    sn4 r2 sn4
    sn4 sn4 r2
    sn2 r2
    sn2 sn2
    \break
    \bar "|."
  }
}
% Score:1 ends here
