# Elementary studies for snare drum

This is an open source [snare drum
book](https://leanpub.com/drumbook_1) written in plain text and built
using free software.

## Dependencies

lilypond, latex, and make.

## Usage

Run `make` to build the PDF.

