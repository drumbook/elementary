% [[file:helper.org::*Header][Header:1]]
\include "definitions.ly"
\header {
  title = "1"
}
% Header:1 ends here

% [[file:helper.org::*Score][Score:1]]
%{
Initial input: \time 4/4 '(4 4 4 4 4 4 4), 16, 5
%}
\version "2.20.0"
\score {
  \drums {
    \time 4/4
    sn4 r4 sn4 sn4
    r4 sn4 sn4 r4
    sn4 sn4 sn4 r4
    sn4 r4 sn4 sn4
    \break
    sn4 sn4 sn4 sn4
    r4 sn4 sn4 sn4
    sn4 sn4 sn4 sn4
    sn4 sn4 sn4 r4
    \break
    r4 sn4 sn4 r4
    sn4 sn4 sn4 sn4
    sn4 sn4 sn4 sn4
    sn4 r4 sn4 sn4
    \break
    sn4 r4 sn4 sn4
    r4 sn4 sn4 sn4
    sn4 sn4 r4 sn4
    sn4 sn4 sn4 sn4
    \break
    r2 sn4 r4
    sn4 sn4 sn4 sn4
    r4 sn4 sn4 r4
    sn4 sn4 sn4 sn4
    \break
    sn4 r2 sn4
    sn4 sn4 sn4 sn4
    sn4 sn4 sn4 sn4
    sn4 r4 sn4 r4
    \break
    sn4 sn4 sn4 sn4
    sn4 sn4 sn4 sn4
    r2 r4 sn4
    sn4 sn4 sn4 r4
    \break
    sn4 sn4 sn4 sn4
    r4 sn4 sn4 sn4
    sn4 r4 sn4 sn4
    sn4 sn4 r2
    \break
    sn4 r4 sn4 sn4
    sn4 sn4 sn4 r4
    sn4 sn4 sn4 sn4
    \bar "|."
  }
}
% Score:1 ends here
