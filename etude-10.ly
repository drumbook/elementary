% [[file:helper.org::*Header][Header:1]]
\include "definitions.ly"
\header {
  title = "10"
}
% Header:1 ends here

% [[file:helper.org::*Score][Score:1]]
%{
Initial input: \time 4/4 '(4 4 4 8 8 8 8 8 2), 16, 5
%}
\version "2.20.0"
\score {
  \drums {
    \time 4/4
    sn4 sn4 sn8 sn8 sn8 sn8
    sn4 sn8 sn8 sn2
    sn8 r8 sn8 sn8 r2
    sn4 sn8 r8 sn2
    \break
    sn4 sn4 sn8 sn8 sn8 sn8
    sn8 sn8 sn8 r8 sn2
    r4 sn4 sn4 sn8 r8
    sn4 r8 sn8 sn2
    \break
    sn4 sn4 sn8 sn8 sn8 r8
    sn4 sn8 sn8 sn2
    r4 sn8 sn8 sn2
    sn4 sn4 sn4 r8 sn8
    \break
    sn4 sn4 r8 sn8 sn8 r8
    sn4 sn4 sn2
    sn4 r8 sn8 sn2
    sn4 r8 sn8 sn2
    \break
    sn4 sn4 sn8 sn8 sn8 sn8
    sn4 sn8 sn8 sn2
    sn4 sn8 sn8 sn2
    r4 sn4 sn4 r8 sn8
    \break
    sn4 sn8 sn8 sn2
    sn4 sn4 sn4 sn8 sn8
    sn4 sn8 sn8 r2
    r4 sn8 r8 sn2
    \break
    sn4 sn4 sn8 sn8 sn8 sn8
    r4 r8 sn8 sn2
    sn4 sn8 sn8 sn2
    sn4 sn4 r8 sn8 sn8 sn8
    \break
    sn4 sn8 r8 sn2
    sn4 sn4 sn8 sn8 sn8 sn8
    sn8 r4 sn8 sn2
    sn4 sn4 r4 sn8 sn8
    \break
    sn4 sn4 sn4 sn8 r8
    sn4 sn4 r4 r8 sn8
    sn4 sn8 sn8 sn2
    sn4 sn4 sn8 sn8 sn8 r8
    \break
    sn4 sn4 sn2
    sn4 sn4 sn2
    sn4 sn8 sn8 sn2
    r4 sn8 sn8 sn2
    \break
    sn4 r4 sn8 sn8 r8 sn8
    sn4 r4 sn2
    sn4 sn4 sn4 sn8 sn8
    sn4 sn4 r8 sn8 sn8 sn8
    \break
    sn4 sn8 sn8 sn2
    sn4 sn8 sn8 sn2
    sn4 sn8 r8 sn2
    r4 r8 sn8 r2
    \break
    sn4 sn8 sn8 sn2
    sn4 sn4 sn8 r8 sn8 sn8
    sn4 sn8 r8 sn2
    sn8 sn8 r8 sn8 sn2
    \break
    sn4 sn4 sn4 sn8 sn8
    sn4 r4 sn8 r8 sn8 sn8
    sn4 sn4 sn8 r8 sn8 sn8
    sn8 sn8 sn8 sn8 sn2
    \bar "|."
  }
}
% Score:1 ends here
