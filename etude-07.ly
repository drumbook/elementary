% [[file:helper.org::*Header][Header:1]]
\include "definitions.ly"
\header {
  title = "7"
}
% Header:1 ends here

% [[file:helper.org::*Score][Score:1]]
%{
Initial input: \time 3/4 '(4 4 4 4 4 4 2.0 2.0 2 2), 12, 4
%}
\version "2.20.0"
\score {
  \drums {
    \time 3/4
    sn4 r4 sn4
    sn4 r4 sn4
    r4 sn2
    sn4 sn2
    \break
    sn4 sn4 sn4
    r4 sn2
    sn4 r4 sn4
    r4 sn4 r4
    \break
    sn4 sn2
    sn4 sn4 sn4
    sn4 sn4 sn4
    sn4 sn4 sn4
    \break
    sn4 sn2
    r4 sn4 r4
    sn4 sn2
    r4 sn4 r4
    \break
    sn4 sn4 sn4
    sn4 sn2
    sn4 sn2
    r2 sn4
    \break
    sn4 sn4 r4
    sn4. sn4.
    sn4 r2
    sn4 sn4 sn4
    \break
    sn4 sn2
    sn4 sn4 sn4
    sn4 sn2
    sn4 sn4 sn4
    \break
    sn4 r2
    r4 sn2
    sn4 sn4 r4
    sn4 sn4 sn4
    \break
    sn4 sn2
    sn4 r4 sn4
    r4 sn2
    sn4 sn4 sn4
    \bar "|."
  }
}
% Score:1 ends here
