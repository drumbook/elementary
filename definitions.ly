% Style sheet
% http://lilypond.org/doc/v2.20/Documentation/learning/style-sheets

\version "2.20.0"
\paper {
  % Set global fonts
  #(define fonts
    (set-global-fonts
     #:roman "Linux Libertine,serif"
     #:sans "Linux Biolinum,sans-serif"
     #:typewriter "Linux Libertine Mono,monospace"))

  % Suppress page number
  print-page-number = ##f
  ragged-last-bottom = ##f
  tagline = ""

  % Change indentation of the first staff
  indent = #0

  % Set margins
  top-margin   = 20

  % Increase the title's font size
  % http://lilypond.org/doc/v2.20/Documentation/notation/custom-titles-headers-and-footers
  bookTitleMarkup = \markup {
    \column {
      \fill-line {
	\line { \fontsize #8 \bold \fromproperty #'header:title }
      }
      \line { \fromproperty #'header:meter \null }
    }
  }
}

% Set aliases
trill = \startTrillSpan
trillstop = \stopTrillSpan
flam = \drummode { \acciaccatura { sn8 } }
drag = \drummode { \grace { \once \override Beam #'positions = #'(2.5 . 2.5) sn16 ( sn ) } }
dragg = \drummode { \grace { \once \override Beam #'positions = #'(2.5 . 2.5) sn16 ( sn sn ) } }
draggg = \drummode { \grace { \once \override Beam #'positions = #'(2.5 . 2.5) sn16 ( sn sn sn ) } }
